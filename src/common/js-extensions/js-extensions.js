// Minified javascript overloads / extensions here

(function () {
    "use strict";

    /**
     * Array move
     * http://stackoverflow.com/questions/5306680/move-an-array-element-from-one-array-position-to-another
     * E.G. [1, 2, 3].move(0, 1) gives [2, 1, 3]
     * @param  {int} old_index  - index of element to move
     * @param  {int} new_index  - index of position to move element to
     * @return {array}          - new array
     */
    Array.prototype.move = function (old_index, new_index) {
        if (new_index >= this.length) {
            var k = new_index - this.length;
            while ((k--) + 1) {
                this.push(undefined);
            }
        }
        this.splice(new_index, 0, this.splice(old_index, 1)[0]);
        return this; // for testing purposes, splcie will perfom operation in-place
    };


    /**
     * Array Remove - By John Resig (MIT Licensed)
     * http://ejohn.org/blog/javascript-array-remove/
     * @param  {int} from   Start position in array of element(s) to be removed
     * @param  {int} to     End position in array of element(s) to be removed (optional)
     * @return {array}      Modified array
     */
    Array.prototype.remove=function(from,to){var rest=this.slice((to||from)+1||this.length);this.length=from<0?this.length+from:from;return this.push.apply(this,rest);};


    /**
     * Return a random Integer between the given min / max
     *
     * http://stackoverflow.com/questions/4959975/generate-random-value-between-two-numbers-in-javascript
     */
    function randomInt(min,max) {
        return Math.floor(Math.random()*(max-min+1)+min);
    }


    /**
     * Extend String, allow the given value(s) to have their first letter capitalised
     * Can receive string or array
     * @return {string} String received with first character capitalised.
     */
    String.prototype.capitaliseFirstLetter = function() {
        var thisArray = this.split(' ');
        var returnString = '';

        for (var i = 0; i < thisArray.length; i++) {
            returnString += thisArray[i].charAt(0).toUpperCase() + thisArray[i].slice(1) + ' ';
        }

        return returnString.trim();
    };
})();