(function(app) {

    app.config(['$stateProvider', function ($stateProvider) {
        $stateProvider.state('lush', {
            url: '/lush',
            views: {
                "main": {
                    controller: 'lushController',
                    templateUrl: 'lush/lush.tpl.html'
                }
            },
            data:{ pageTitle: 'lush' }
        });
    }]);

    app.controller('lushController', ['$localStorage', 'LushService', '$rootScope', '$scope', function ($localStorage, LushService, $rootScope, $scope) {

        /**
         * Amend the currentLushes array
         * Add a lush
         */
        $scope.addLush = function(lush) {
            if ($scope.lushFrm.$valid) {
                $rootScope.currentLushes.push(lush.moniker);
                LushService.prototype.setLushes($rootScope.currentLushes);
            }
        };


        /**
         * Amend the currentLushes array
         * Remove a lush
         */
        $scope.removeLush = function() {
            $rootScope.currentLushes.remove(0);
            LushService.prototype.setLushes($rootScope.currentLushes);
        };


        /**
         * Clear the currentLushes array
         */
        $scope.removeAllLushes = function() {
            $rootScope.currentLushes = [];
            LushService.prototype.setLushes($rootScope.currentLushes);
        };


        /**
         * initialise the app
         */
        var init = function() {
        };

        init();
    }]);

}(angular.module("whoseRoundIsIt.lush", [
    'ui.router'
])));