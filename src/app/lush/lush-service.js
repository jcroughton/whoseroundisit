(function(app) {

    app.factory('LushService', ['$localStorage', function ($localStorage) {
        var LushService = function(){};

        /**
         * Return the current lushes stored in local storage
         * @return {Array}
         */
        LushService.prototype.getLushes = function() {
            return $localStorage.lushes;
        };


        /**
         * Set local storage value to value provided
         * @param {Array} lushes Array to be set in local storage
         */
        LushService.prototype.setLushes = function(lushes) {
            return !!($localStorage.lushes = lushes);
        };


        /**
         * Return the current lush mode from local storage
         * @return {Bool} Bool representation of local storage value
         */
        LushService.prototype.getLushMode = function() {
            return !!($localStorage.lushMode);
        };


        /**
         * Set the local storage for lush mode to value provided
         * @param {Bool} mode
         */
        LushService.prototype.setLushMode = function(mode) {
            return !!($localStorage.lushMode = mode);
        };

        return LushService;
    }]);

}(angular.module("whoseRoundIsIt.LushServices", [
    'ngStorage'
])));