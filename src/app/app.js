(function(app) {

    app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/home');
    }]);

    app.run(function () {});

    /**
     * Get a randomly selected element from the appropriate array
     * @return {string} - The contents of the array element chosen
     */
    app.directive('randomSwearWord', function() {
        return {
            restrict: 'A',
            link: function (scope, element, attrs, controller) {

                var arrayToUse = null;
                if (attrs.randomSwearWord === 'ed') {
                    arrayToUse = scope.lushEdArray;
                } else {
                    arrayToUse = scope.lushIngArray;
                }

                var min = 0;
                var max = arrayToUse.length-1;
                var pos = Math.floor(Math.random()*(max-min+1)+min);
                var word = arrayToUse[pos];
                var capFirst = ( attrs.cap === 'true' );

                if (capFirst) {
                    word = word.capitaliseFirstLetter();
                }

                element.html(word);
            }
        };
    });


    /**
     * Select the content of an input when it receives 'focus' (is clicked or
     * tapped in).
     * Adapted from http://stackoverflow.com/questions/14995884/select-text-on-input-focus
     */
    app.directive('selectOnFocus', function () {
        return {
            restrict: 'A',
            link: function (scope, element) {
                var focusedElement;
                element.on('click touchstart', function () {
                    if (focusedElement != this) {
                        this.select();
                        focusedElement = this;
                    }
                });
                element.on('blur', function () {
                    focusedElement = null;
                });
            }
        };
    });


    app.controller('AppController', ['$localStorage', 'LushService', '$rootScope', '$scope', function ($localStorage, LushService, $rootScope, $scope) {
        // rootScope var to store curernt lushes (people drinking)
        $rootScope.currentLushes = LushService.prototype.getLushes() || [];

        // scaope var to store current lush mode
        $scope.lushMode = LushService.prototype.getLushMode();

        // Lush 'Ing' swear word array
        $scope.lushIngArray = [
            'fackin\'',
            'fucking',
            'cocking',
            'shitting',
            'cock sucking'
        ];

        // Lush 'Ed' swear word array
        $scope.lushEdArray = [
            'facked',
            'cocked',
            'shitted',
            'dicked',
            'fucked'
        ];


        /**
         * App method to allow call of service method to set lush mode
         */
        $scope.setLushMode = function(lushMode) {
            LushService.prototype.setLushMode(lushMode);
        };


        /**
         * initialise the app
         */
        var init = function() {
        };

        init();

    }]);

}(angular.module('whoseRoundIsIt', [
    'whoseRoundIsIt.home',
    'templates-app',
    'templates-common',
    'ui.router.state',
    'ui.router',
    'whoseRoundIsIt.lush',
    'whoseRoundIsIt.LushServices',
    'NgSwitchery',
    'ngStorage',
])));
