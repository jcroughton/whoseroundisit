/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * 'src/app/home', however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a 'note' section could have the submodules 'note.create',
 * 'note.delete', 'note.edit', etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 */
(function(app) {

    app.config(['$stateProvider', function ($stateProvider) {
        $stateProvider.state('home', {
            url: '/home',
            views: {
                "main": {
                    controller: 'HomeController',
                    templateUrl: 'home/home.tpl.html'
                }
            },
            data:{ pageTitle: 'Home' }
        });
    }]);

    // As you add controllers to a module and they grow in size, feel free to place them in their own files.
    //  Let each module grow organically, adding appropriate organization and sub-folders as needed.
    app.controller('HomeController', ['LushService', '$rootScope', '$scope', function (LushService, $rootScope, $scope) {
        /**
         * Amend the currentLushes array
         * Move position 0 to last position
         */
        $scope.nextLush = function() {
            $rootScope.currentLushes.move(0, $rootScope.currentLushes.length-1);
        };

        /**
         * initialise the app
         */
        var init = function($scope) {
            $rootScope.currentLushes = LushService.prototype.getLushes() || [];
        };

        init($scope);
    }]);

// The name of the module, followed by its dependencies (at the bottom to facilitate enclosure)
}(angular.module("whoseRoundIsIt.home", [
    'ui.router'
])));